using System;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;


// BROKEN
// Adds a domain user successfully, but adding to domain admins group doesn't work ... someone else than me should fix...
// See line 101 for error message
// Please submit a Pull request once you figure this shit out...

namespace AddUser
{
    class Program
    {
        static void Main(string[] args)
        {
            if(args.Length < 3 || args.Length > 4 || args[0] == "/help" || args[0] == "/h" || args[0] == "/?" || args[0] == "-help" || args[0] == "-h" || args[0] == "--help")
            {
                Usage();
                return;
            }
            else
            {
                bool domaincontext = false;
                bool localcontext = false;
                bool addmin = false;
                string username = null;
                string password = null;
                foreach(string arg in args)
                {
                    if(arg == "--domain")
                    {
                        domaincontext = true;
                    }
                    else if(arg == "--local")
                    {
                        localcontext = true;
                    }
                    else if(arg == "--admin")
                    {
                        addmin = true;
                    }
                    else
                    {
                        if(username == null)
                        {
                            username = arg;
                        }
                        else if(password == null)
                        {
                            password = arg;
                        }
                    }
                }
                if((domaincontext && localcontext) || !(domaincontext || localcontext)) // either neither --domain or --local was specified or BOTH were specified ???
                {
                    Usage();
                    return;
                }
                else if(localcontext)
                {
                    CreateLocalUser(username, password, username, addmin);
                }
                else if(domaincontext)
                {
                    CreateDomainUser(username, password);
                    
                    AddDomainUserToGroup(username, "testgrp");
                }
            }
        }
        static void Usage()
        {
            Console.WriteLine("AddUser -- Creates a new local or domain user account, optionally adding it to the local or domain admins group");
            Console.WriteLine("Usage: AddUser.exe <--local|--domain> [--admin] <username> <password>");
            Console.WriteLine("Examples: AddUser.exe --local testuserlocal password123!");
            Console.WriteLine("Examples: AddUser.exe --domain domuser password1234!");
            Console.WriteLine("Examples: AddUser.exe --local --admin mylocaladmin P@ssw0rd");
            Console.WriteLine("Examples: AddUser.exe --domain --admin cla2021ept SecurePassword1234_!");
        }
        static void CreateDomainUser(string accountname, string password)
        {
            using (var pc = new PrincipalContext(ContextType.Domain))
            {
                using (var up = new UserPrincipal(pc))
                {
                    up.SamAccountName = accountname;
                    up.EmailAddress = accountname + "@" + Environment.UserDomainName;
                    up.SetPassword(password);
                    up.Enabled = true;
                    up.Save();
                }
            }
        }
        static void AddDomainUserToGroup(string userId, string groupName)
        {
            using (PrincipalContext pc = new PrincipalContext(ContextType.Domain))
            {
                GroupPrincipal group = GroupPrincipal.FindByIdentity(pc, groupName);
                Console.WriteLine(group.DisplayName);
                group.Members.Add(pc, IdentityType.UserPrincipalName, userId); // Doesn't FUCKING WORK!
                // System.DirectoryServices.AccountManagement.NoMatchingPrincipalException: No principal matching the specified parameters was found

                group.Save();
            }

        }
        // Yoink: https://forums.asp.net/t/1057619.aspx?How+do+we+create+a+user+with+admin+rights+in+C+using+DirectoryEntry
        static void CreateLocalUser(string login, string password, string fullName, bool isAdmin)
        {
            try
            {
                DirectoryEntry dirEntry = new DirectoryEntry("WinNT://" + Environment.MachineName + ",computer");
                DirectoryEntries entries = dirEntry.Children;
                DirectoryEntry newUser = entries.Add(login, "user");
                newUser.Properties["FullName"].Add(fullName);
                newUser.Invoke("SetPassword", password);
                newUser.CommitChanges();


                // Remove the if condition along with the else to create user account in "user" group.
                DirectoryEntry grp;
                if (isAdmin)
                {
                    grp = dirEntry.Children.Find("Administrators", "group");
                    if (grp != null) { grp.Invoke("Add", new object[] { newUser.Path.ToString() }); }
                }
                else
                {
                    grp = dirEntry.Children.Find("Users", "group");
                    if (grp != null) { grp.Invoke("Add", new object[] { newUser.Path.ToString() }); }
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
