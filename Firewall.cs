using System;
using System.Linq;
using WindowsFirewallHelper;


// Library used: https://github.com/falahati/WindowsFirewallHelper (Installed from NuGet)

// Make sure to ILMerge.exe your resulting binary with the WindowsFirewallHelper.dll otherwise it won't work with execute-assembly or csharp
namespace Firewall
{
    public class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length == 0 || args[0] == "-h" || args[0] == "/?" || args[0] == "/h" || args[0] == "--help" || args[0] == "/help")
            {
                Usage();
            }
            else if (args[0] == "list")
            {
                if (args.Length == 2)
                {
                    // list rulename
                    IRule rule = FirewallManager.Instance.Rules.SingleOrDefault(r => r.Name == args[1]);
                    if (rule != null)
                    {
                        try
                        {
                            Console.WriteLine("[*] Listing single rule: \"" + args[1] + "\"");
                            Console.WriteLine("==========================================");
                            Console.WriteLine("Name:\t\t" + rule.Name);
                            Console.WriteLine("Enabled:\t" + rule.IsEnable);
                            Console.WriteLine("Action:\t\t" + rule.Action);
                            Console.WriteLine("Port:\t\t" + string.Join(" ", rule.LocalPorts));
                            Console.WriteLine("Profile:\t" + rule.Profiles);
                            Console.WriteLine("==========================================");

                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("[!] Encountered error while trying to list rule: " + e.Message);
                        }
                    }
                    else
                    {
                        Console.WriteLine("[!] No rule with specified name.. (Did you typo?) : \"" + args[1] + "\"");
                    }
                }
                else
                {
                    // list all rules
                    try
                    {
                        IRule[] rules = FirewallManager.Instance.Rules.ToArray();
                        Console.WriteLine("[*] Listing all rules...");
                        Console.WriteLine("==========================================");
                        foreach (IRule rule in rules)
                        {
                            Console.WriteLine("Name:\t\t" + rule.Name);
                            Console.WriteLine("Enabled:\t" + rule.IsEnable);
                            Console.WriteLine("Action:\t\t" + rule.Action);
                            Console.WriteLine("Port:\t\t" + string.Join(" ", rule.LocalPorts));
                            Console.WriteLine("Profile:\t" + rule.Profiles);
                            Console.WriteLine("==========================================");
                        }
                        Console.WriteLine("[*] Rule listing complete!");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("[!] Encountered error while trying to list rules: " + e.Message);
                    }
                }
            }
            else if (args[0] == "addport")
            {
                if (args.Length != 3)
                {
                    Console.WriteLine("[!] Add rules usage requires exactly 3 arguments. See help info below:\n");
                    Usage();
                    return;
                }
                else
                {
                    // add a new rule based on port
                    int.TryParse(args[2], out int port);
                    if (port != 0)
                    {
                        IRule rule = FirewallManager.Instance.CreatePortRule(
                            (FirewallProfiles)7, // all profiles (domain, local, public)
                            args[1], // name
                            FirewallAction.Allow,
                            (ushort)port,
                            FirewallProtocol.TCP
                        );
                        try
                        {
                            FirewallManager.Instance.Rules.Add(rule);
                            Console.WriteLine("[*] Added rule successfully: \"" + args[1] + "\" | TCP/" + port);
                        }
                        catch(Exception e)
                        {
                            Console.WriteLine("[!] Encountered error while trying to add rule:" + e.Message);
                        }
                    }
                    else
                    {
                        Console.WriteLine("[!] Failed to parse port number. Is this a number? : " + args[2]);
                    }
                }
            }
            else if (args[0] == "addapp")
            {
                if (args.Length != 3)
                {
                    Console.WriteLine("[!] Add rules usage requires exactly 3 arguments. See help info below:\n");
                    Usage();
                    return;
                }
                else
                {
                    // add a new rule based on appname
                    IRule rule = FirewallManager.Instance.CreateApplicationRule(
                        (FirewallProfiles)7, // all profiles (domain, local, public)
                        args[1], // name
                        FirewallAction.Allow,
                        args[2], // appname
                        FirewallProtocol.TCP
                    );
                    try
                    {
                        FirewallManager.Instance.Rules.Add(rule);
                        Console.WriteLine("[*] Added rule successfully: \"" + args[1] + "\" | Application: " + args[2]);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("[!] Encountered error while trying to add rule:" + e.Message);
                    }

                }
            }
            else if (args[0] == "remove")
            {
                if (args.Length != 2)
                {
                    Console.WriteLine("[!] Remove rule usage requires exactly 2 arguments. See help info below:\n");
                    Usage();
                    return;
                }
                else
                {
                    // remove a rule by name
                    IRule rule = FirewallManager.Instance.Rules.SingleOrDefault(r => r.Name == args[1]); // Fix not being able to handle more than one instance of a rule with the same name :( I hate LINQ
                    if (rule != null)
                    {
                        try
                        {
                            FirewallManager.Instance.Rules.Remove(rule);
                            Console.WriteLine("[*] Removed rule successfully: \"" + args[1] + "\"");
                        }
                        catch(Exception e)
                        {
                            Console.WriteLine("[!] Encountered error while trying to remove rule:" + e.Message);
                        }
                    }
                    else
                    {
                        Console.WriteLine("[!] No rule with specified name.. (Did you typo?) : \"" + args[1] + "\"");
                    }
                }
            }
            else
            {
                Console.WriteLine("[!] Invalid arguments provided. See the help below for usage information:\n");
                Usage();
            }
        }

    static void Usage()
        {
            Console.WriteLine("Firewall.exe list                                                -- shows current firewall rules");
            Console.WriteLine("Firewall.exe list \"firewall rule name\"                         -- shows infor about specified firewall rule");
            Console.WriteLine("Firewall.exe addport \"firewall rule name\" port                 -- adds a new rule allowing connections to specified port");
            Console.WriteLine("Firewall.exe addapp \"firewall rule name\" \"Full path to exe\"  -- adds a new rule allowing specified exe to bind to ports");
            Console.WriteLine("Firewall.exe remove \"firewall rule name\"                       -- removes the rule with the specified name");
        }
    }
}
