using System;
using System.Security.Principal;

// Checks if the user is currently running in a high integrity context
namespace AmIHighRightNow
{
    public class Program
    {
        public static void Main(string[] args)
        {
            if(AmIHigh())
            {
                Console.WriteLine("[+] You are high right now (high integrity)");
            }
            else
            {
                Console.WriteLine("[-] You are not high right now (medium integrity)");
            }
        }

        private static bool AmIHigh()
        {
            // returns true if the current process is running with adminstrative privs in a high integrity context
            WindowsIdentity identity = WindowsIdentity.GetCurrent();
            WindowsPrincipal principal = new WindowsPrincipal(identity);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }
    }
}
